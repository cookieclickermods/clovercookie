/***********
 * Prepare *
 ***********/
CloverCookie = {};
CloverCookie.Config = {};
CloverCookie.Feature = {};
CloverCookie.Options = [];
CloverCookie.Display = {};
CloverCookie.Utility = {};
CloverCookie.ConfigPrefix = "CloverConfig";

/***********
 * Display *
 ***********/
CloverCookie.Display.UpdateMenu = () => {
	if (Game.onMenu !== "prefs") return;

	const section = document.createElement("div");
	section.className = "subsection";

	const title = document.createElement("div");
	title.className = "title";
	title.textContent = "CloverCookie";

	const toggleListing = CloverCookie.Display.CreateToggleListing;
	const numberListing = CloverCookie.Display.CreateNumberListing;

	section.appendChild(title);
	for (const option of CloverCookie.Options) {
		switch (option.type) {
			case "toggle":
				section.appendChild(toggleListing(option));
				break;
			case "number":
				section.appendChild(numberListing(option));
				break;
		}
	}

	const menu = document.querySelector("#menu>.block");
	menu.prepend(section);
}

CloverCookie.Display.CreateToggleListing = (option) => {
	const id = option.id;
	const isEnabled = CloverCookie.Config[id];

	const div = document.createElement("div");
	div.className = "listing";

	const adjust = (elem, enabled) => {
		const suffix = enabled ? "ON" : "OFF";
		elem.className = enabled ? "option" : "option off";
		elem.textContent = `${option.name} ${suffix}`;
	};

	const a = document.createElement("a");
	a.id = CloverCookie.ConfigPrefix + option.name;
	adjust(a, isEnabled);
	a.onclick = (e) => {
		const t = e.target;
		const isEnabled = !CloverCookie.Config[id];
		adjust(t, isEnabled);

		CloverCookie.Config[id] = isEnabled;
		CloverCookie.SaveConfig(CloverCookie.Config);
	};

	const label = document.createElement("label");
	label.textContent = option.desc;

	div.appendChild(a);
	div.appendChild(label);

	return div;
}

CloverCookie.Display.CreateNumberListing = (option) => {
	const id = option.id;
	const value = CloverCookie.Config[id];

	const div = document.createElement("div");
	div.className = "listing";

	const span = document.createElement("span");
	span.className = "option";
	span.textContent = option.name;
	span.style.paddingRight = "8px";

	const input = document.createElement("input");
	input.id = CloverCookie.ConfigPrefix + option.name;
	input.className = "option";
	input.type = "number";
	input.min = option.min;
	input.max = option.max;
	input.value = value;
	input.onchange = (e) => {
		const t = e.target;
		const min = Number(t.min);
		const max = Number(t.max);
		let n = Number(t.value);
		if (n < min) n = min;
		if (n > max) n = max;
		t.value = n;

		CloverCookie.Config[id] = n;
		CloverCookie.SaveConfig(CloverCookie.Config);
	};

	const label = document.createElement("label");
	label.textContent = option.desc;

	div.appendChild(span);
	div.appendChild(input);
	div.appendChild(label);

	return div;
}

/**********
 * Config *
 **********/
CloverCookie.SaveConfig = (config) => {
	localStorage.setItem(CloverCookie.ConfigPrefix, JSON.stringify(config));
}

CloverCookie.LoadConfig = () => {
	if (!localStorage.getItem(CloverCookie.ConfigPrefix)) {
		CloverCookie.SaveConfig(CloverCookie.ConfigDefault());
		CloverCookie.LoadConfig();
		return;
	}

	CloverCookie.Config = JSON.parse(localStorage.getItem(CloverCookie.ConfigPrefix));
}

CloverCookie.Options = [
	{
		id: "AutoClick",
		name: "AutoClick",
		desc: "Click the big cookie every frame",
		type: "toggle",
		default: true,
	},
	{
		id: "AutoShimmer",
		name: "AutoShimmer",
		desc: "Click golden cookies automatically",
		type: "toggle",
		default: true,
	},
	{
		id: "ClickWraths",
		name: "ClickWraths",
		desc: "Click wrath cookies as well",
		type: "toggle",
		default: false,
	},
	{
		id: "AutoPledge",
		name: "AutoPledge",
		desc: "Buy the elder pledge automatically",
		type: "toggle",
		default: false,
	},
	{
		id: "AutoFortune",
		name: "AutoFortune",
		desc: "Click fortune messages automatically",
		type: "toggle",
		default: true,
	},
	{
		id: "AutoPop",
		name: "AutoPop",
		desc: "Pop wrinklers automatically",
		type: "toggle",
		default: true,
	},
	{
		id: "AutoGodzamok",
		name: "AutoGodzamok",
		desc: "During a frenzy and any building special: cast a click frenzy, sell and rebuy all farms and turn on the golden switch while click frenzy is active",
		type: "toggle",
		default: true,
	},
	{
		id: "MinBuildingAmount",
		name: "Minimum buildings:",
		desc: "Minimum amount of buildings required in order to trigger AutoGodzamok",
		type: "number",
		default: 100,
		min: 1,
		max: 2000,
	},
	{
		id: "AutoAscend",
		name: "AutoAscend",
		desc: "Automatically ascends and reincarnetes after one prestige is gained in the current run",
		type: "toggle",
		default: false,
	},
	{
		id: "AutoLump",
		name: "AutoLump",
		desc: "Automatically clicks mature sugar lumps and reloads the game until the maximum amount possible is harvested",
		type: "toggle",
		default: true,
	},
	{
		id: "AutoStock",
		name: "AutoStock",
		desc: "Automatically buys or sells stock while trying to be as profitable as possible",
		type: "toggle",
		default: true,
	},
	{
		id: "AutoPlant",
		name: "AutoPlant",
		desc: "Automatically plants the selected seed on all empty tiles",
		type: "toggle",
		default: false,
	},
	{
		id: "SeedType",
		name: "Seed type:",
		desc: "Seed type to plant automatically",
		type: "number",
		default: 0,
		min: 0,
		max: 33,
	},
]

CloverCookie.ConfigDefault = () => {
	const cfg = {};
	for (const option of CloverCookie.Options)
		cfg[option.id] = option.default;

	return cfg;
}

/*********
 * Utility *
 **********/
CloverCookie.Utility.GetNextFTHOFSpell = () => {
	const M = Game.Objects["Wizard tower"].minigame;
	let backfire = M.getFailChance(M.spellsById[1]);
	let FTHOFcookie = "";

	Math.seedrandom(Game.seed + "/" + M.spellsCastTotal);
	roll = Math.random();

	backfire += 0.15 * Game.shimmers.length;
	const idx = ((Game.season === "valentines" || Game.season === "easter") ? 1 : 0) + ((Game.chimeType === 1 && Game.ascensionMode !== 1) ? 1 : 0);

	if (roll < (1 - backfire)) {
		if (idx > 0) Math.random();
		if (idx > 1) Math.random();
		Math.random();
		Math.random();

		let choices = [];
		choices.push("Frenzy", "Lucky");
		if (!Game.hasBuff("Dragonflight")) choices.push("Click Frenzy");
		if (Math.random() < 0.1) choices.push("Cookie Storm", "Cookie Storm", "Blab");
		if (Game.BuildingsOwned >= 10 && Math.random() < 0.25) choices.push("Building Special");
		if (Math.random() < 0.15) choices = ["Cookie Storm Drop"];
		if (Math.random() < 0.0001) choices.push("Free Sugar Lump");

		FTHOFcookie = choose(choices);
	} else {
		if (idx > 0) Math.random();
		if (idx > 1) Math.random();
		Math.random();
		Math.random();

		let choices = [];
		choices.push("Clot", "Ruin");
		if (Math.random() < 0.1) choices.push("Cursed Finger", "Elder Frenzy");
		if (Math.random() < 0.003) choices.push("Free Sugar Lump");
		if (Math.random() < 0.1) choices = ["Blab"];

		FTHOFcookie = choose(choices);
	}

	Math.seedrandom();
	return FTHOFcookie;
}

CloverCookie.Utility.TriggerGodzamok = () => {
	const T = Game.Objects["Temple"].minigame;

	if (!Game.hasBuff("Devastation") && (T.slot[0] === 8 || T.slot[1] === 8 || T.slot[2] === 8)) {
		const farmAmount = Game.ObjectsById[2].amount;
		Game.ObjectsById[2].sell(farmAmount);
		Game.ObjectsById[2].buy(farmAmount);
	}
}

CloverCookie.Utility.ToggleGoldenSwitch = () => {
	if (!Game.UpgradesById[327].unlocked) return;

	const switchOff = Game.UpgradesById[331];
	const switchOn = Game.UpgradesById[332];

	if (switchOff.bought) switchOn.buy();
	else switchOff.buy();
}

CloverCookie.Utility.CastUntilClickFrenzy = () => {
	const M = Game.Objects["Wizard tower"].minigame;
	if (CloverCookie.Utility.GetNextFTHOFSpell() !== "Click Frenzy" && CloverCookie.Utility.IsMagicFull()) {
		M.castSpell(M.spellsById[4]);
	}
}

CloverCookie.Utility.IsSpellCastable = (spellId) => {
	const M = Game.Objects["Wizard tower"].minigame;
	const spell = M.spellsById[spellId];
	const requiredMagic = spell.costMin + spell.costPercent * M.magicM;
	return (M.magic - requiredMagic) >= 0;
}

CloverCookie.Utility.IsMagicFull = () => {
	const M = Game.Objects["Wizard tower"].minigame;
	return M.magicM === M.magic;
}

CloverCookie.Utility.GetCurrentBuildingBuff = () => {
	for (const buffs of Object.values(Game.goldenCookieBuildingBuffs))
		if (Game.hasBuff(buffs[0]))
			return Game.buffs[buffs[0]];

	return undefined;
}

CloverCookie.Utility.GetBuffDurationMultiplier = () => {
	let effectDurMod = 1;
	if (Game.Has("Get lucky")) effectDurMod *= 2;
	if (Game.Has("Lasting fortune")) effectDurMod *= 1.1;
	if (Game.Has("Lucky digit")) effectDurMod *= 1.01;
	if (Game.Has("Lucky number")) effectDurMod *= 1.01;
	if (Game.Has("Green yeast digestives")) effectDurMod *= 1.01;
	if (Game.Has("Lucky payout")) effectDurMod *= 1.01;
	effectDurMod *= 1 + Game.auraMult("Epoch Manipulator") * 0.05;
	effectDurMod *= Game.eff("goldenCookieEffDur"); // We assume that its not a wrath cookie

	if (Game.hasGod) {
		const godLvl = Game.hasGod("decadence");
		if (godLvl === 1) effectDurMod *= 1.07;
		else if (godLvl === 2) effectDurMod *= 1.05;
		else if (godLvl === 3) effectDurMod *= 1.02;
	}

	return effectDurMod;
}

CloverCookie.Utility.CalculateMultCps = (buildingAmount) => buildingAmount / 10 + 1

CloverCookie.Utility.RunGodzamokProcess = () => {
	const M = Game.Objects["Wizard tower"].minigame;
	const clickFrenzyTime = 13 * CloverCookie.Utility.GetBuffDurationMultiplier() * Game.fps;
	const buildingBuff = CloverCookie.Utility.GetCurrentBuildingBuff();
	const minMultCps = CloverCookie.Utility.CalculateMultCps(CloverCookie.Config.MinBuildingAmount);

	if (Game.hasBuff("Frenzy") && buildingBuff) {

		if (Game.buffs["Frenzy"].time >= clickFrenzyTime && buildingBuff.time >= clickFrenzyTime) {

			if (CloverCookie.Utility.GetNextFTHOFSpell() === "Click Frenzy" &&
				!Game.hasBuff("Click frenzy") &&
				CloverCookie.Utility.IsSpellCastable(1) &&
				buildingBuff.multCpS >= minMultCps) {

				CloverCookie.Utility.ToggleGoldenSwitch();
				M.castSpell(M.spellsById[1]);
			}
		}
		if (Game.hasBuff("Click frenzy")) {
			CloverCookie.Utility.TriggerGodzamok();
		}
	} else if ((!Game.hasBuff("Frenzy") || !buildingBuff) && Game.UpgradesById[331].bought) {
		CloverCookie.Utility.ToggleGoldenSwitch();
	}
}

CloverCookie.Utility.HarvestMaxLumps = () => {
	const save = Game.WriteSave(1);
	const currentLumps = Game.lumps;
	const lumpType = Game.lumpCurrentType;
	Game.clickLump();

	switch (lumpType) {
		// Normal (1)
		case 0:
			if (currentLumps + 1 === Game.lumps) return true;
			Game.ImportSaveCode(save);
			return false;

		// Bifurcated (1-2)
		case 1:
			if (currentLumps + 2 === Game.lumps) return true;
			Game.ImportSaveCode(save);
			return false;

		// Golden (2-7)
		case 2:
			if (currentLumps + 7 === Game.lumps) return true;
			Game.ImportSaveCode(save);
			return false;

		// Meaty (0-2)
		case 3:
			if (currentLumps + 2 === Game.lumps) return true;
			Game.ImportSaveCode(save);
			return false;

		// Caramelized (1-3)
		case 4:
			if (currentLumps + 3 === Game.lumps) return true;
			Game.ImportSaveCode(save);
			return false;
	}
}

CloverCookie.Utility.GetBestBuyValue = (goodId) => {
	// check data dumps to understand why these values were chosen
	const restingValue = 10 + 10 * goodId + (Game.Objects["Bank"].level - 1);

	if (restingValue <= 200) return restingValue / 2;
	else return restingValue - 75;
}

CloverCookie.Utility.GetBestSellValue = (goodId) => {
	// check data dumps to understand why these values were chosen
	const restingValue = 10 + 10 * goodId + (Game.Objects["Bank"].level - 1);

	if (restingValue <= 50 || restingValue > 200) return restingValue + 20;
	else return restingValue * 1.025;
}

CloverCookie.Utility.BuyOrSellProfitable = () => {
	const B = Game.Objects["Bank"].minigame;

	for (let goodId = 0; goodId < B.goodsById.length; goodId++) {
		const good = B.goodsById[goodId];
		const stockLeftToBuy = B.getGoodMaxStock(good) - good.stock;

		if (stockLeftToBuy > 0 && good.val <= CloverCookie.Utility.GetBestBuyValue(goodId))
			B.buyGood(goodId, stockLeftToBuy);
		else if (good.stock > 0 && good.val >= CloverCookie.Utility.GetBestSellValue(goodId))
			B.sellGood(goodId, good.stock);
	}
}

CloverCookie.Utility.PlantSeeds = () => {
	const G = Game.Objects["Farm"].minigame;
	const wantedPlant = CloverCookie.Config.SeedType;
	if (!G.plantsById[wantedPlant].plantable) return;

	for (let y = 0; y < G.plot.length; y++) {
		const row = G.plot[y];
		for (let x = 0; x < row.length; x++) {
			const tile = row[x][0];

			if (!G.isTileUnlocked(x, y)) continue;
			if (tile - 1 === wantedPlant) continue;

			if (tile !== 0) G.useTool(-1, x, y);
			G.useTool(wantedPlant, x, y);
		}
	}
}

/***********
 * Feature *
 ***********/
CloverCookie.Feature.AutoClick = () => {
	if (!CloverCookie.Config.AutoClick) return;

	Game.ClickCookie();
}

CloverCookie.Feature.AutoShimmer = () => {
	if (!CloverCookie.Config.AutoShimmer) return;

	Game.shimmers.forEach((shimmer) => {
		if (!CloverCookie.Config.ClickWraths) {
			if ((shimmer.type === "golden" && shimmer.wrath === 0) || shimmer.type === "reindeer")
				shimmer.pop();
		} else {
			if (shimmer.type === "golden" || shimmer.type === "reindeer")
				shimmer.pop();
		}
	});
}

CloverCookie.Feature.AutoPledge = () => {
	if (!CloverCookie.Config.AutoPledge) return;

	if (Game.UpgradesById[74].unlocked && !Game.UpgradesById[74].bought) {
		Game.UpgradesById[74].buy();
	}
}

CloverCookie.Feature.AutoFortune = () => {
	if (!CloverCookie.Config.AutoFortune) return;

	if (Game.TickerEffect && Game.TickerEffect.type === "fortune") {
		Game.tickerL.click();
	}
}

CloverCookie.Feature.AutoPop = () => {
	if (!CloverCookie.Config.AutoPop) return;

	Game.wrinklers.forEach((wrinkler) => {
		if (wrinkler.close === 1)
			wrinkler.hp = 0;
	});
}

CloverCookie.Feature.AutoGodzamok = () => {
	if (!CloverCookie.Config.AutoGodzamok) return;

	if (Game.Objects["Wizard tower"].minigameLoaded && Game.Objects["Temple"].minigameLoaded) {
		CloverCookie.Utility.CastUntilClickFrenzy();
		CloverCookie.Utility.RunGodzamokProcess();
	}
}

CloverCookie.Feature.AutoAscend = () => {
	if (!CloverCookie.Config.AutoAscend) return;

	if (Game.OnAscend) {
		Game.Reincarnate(1);
	} else if (Game.ascendMeterLevel > 0 && Game.AscendTimer === 0) {
		Game.Ascend(1);
	}
}

CloverCookie.Feature.AutoLump = () => {
	if (!CloverCookie.Config.AutoLump) return;

	const age = Date.now() - Game.lumpT;
	if (age >= Game.lumpMatureAge) {
		CloverCookie.Utility.HarvestMaxLumps();
	}
}

CloverCookie.Feature.AutoStock = () => {
	if (!CloverCookie.Config.AutoStock) return;

	CloverCookie.Utility.BuyOrSellProfitable();
}

CloverCookie.Feature.AutoPlant = () => {
	if (!CloverCookie.Config.AutoPlant) return;
	if (!Game.Objects["Farm"].minigameLoaded) return;

	if (Game.T % Game.fps === 0) {
		CloverCookie.Utility.PlantSeeds();
	}
}

/********
 * Main *
 ********/
CloverCookie.Loop = () => {
	CloverCookie.Feature.AutoClick();
	CloverCookie.Feature.AutoShimmer();
	CloverCookie.Feature.AutoPledge();
	CloverCookie.Feature.AutoFortune();
	CloverCookie.Feature.AutoPop();
	CloverCookie.Feature.AutoGodzamok();
	CloverCookie.Feature.AutoAscend();
	CloverCookie.Feature.AutoLump();
	CloverCookie.Feature.AutoPlant();
}

CloverCookie.Run = () => {
	const id = setInterval(() => {

		const gameLoop = Game.Loop;
		Game.Loop = () => {
			gameLoop();
			CloverCookie.Loop();
		}

		if (Game.Objects["Bank"].minigameLoaded) {
			const stockMarket = Game.Objects["Bank"].minigame;
			const stockMarketTick = stockMarket.tick;
			stockMarket.tick = () => {
				stockMarketTick();
				CloverCookie.Feature.AutoStock();
			}
		}

		const updateMenu = Game.UpdateMenu;
		Game.UpdateMenu = () => {
			updateMenu();
			CloverCookie.Display.UpdateMenu();
		}

		CloverCookie.LoadConfig();
		Game.Win("Third-party");
		clearInterval(id);
	}, 500);
}

/********
 * Load *
 ********/
CloverCookie.Run();